#!/bin/bash

echo -e "\n\033[31mMise à jour du système\033[0m\n"
apt-get install sudo apt update
apt-get upgrade
echo -e "\033[1;32m Mise à jour du système réussi \033[0m \n"

echo -e "\n \033[31m Installation de MySQL \033[0m \n"
apt-get install mysql-client mysql-server
echo -e "\033[1;32m Installation MySQL réussi \033[0m \n"

echo -e "\n \033[31m Installation de Git \033[0m \n"
apt-get install git
echo -e "\033[1;32m Installation Git réussi \033[0m \n"

echo -e "\n \033[31m Installation de phpMyAdmin \033[0m \n"
apt-get install phpmyadmin
ln -s /usr/share/phpmyadmin/ /var/www/html/phpmyadmin
echo -e "\033[1;32m Installation phpMyAdmin réussi \033[0m \n"

echo -e "Récupération des fichiers\n"
git clone https://gitlab.com/AnselmeSDR/auto-install-FiveMServer.git
mv ./auto-install-FiveMServer/* ./
rm -rf auto-install-FiveMServer

read -p 'Entrer le nom de votre serveur: ' dossier
mkdir $dossier
mv fx $dossier/
cd $dossier
chmod -R 755 *

echo -e "\033[31mVotre serveur est presque prêt, modifiez dans le fichier config.cfg les lignes suivantes:\n\nadresse IP de votre serveur\nset mysql_connection_string\nadd_principal identifier.steam:110000112345678 group.admin\nsv_licenseKey\n\033[0m"
read -p 'Appuyez sur entrée pour valider' -s toto
nano ./server_data/config.cfg
echo -e "\nUtilisez la commande suivante pour démarrer le serveur: ./run.sh +exec server.cfg\n"
